/*****
 * Copyright 2018 ONERA
 *
 * This file is part of the MAUVE OPENNI project.
 *
 * MAUVE OPENNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE OPENNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
*****/
#include "openni/mauve/OpenNI.hpp"

using namespace mauve::runtime;
using namespace openni::mauve;
using namespace mauve::types::sensor;

struct OpenNITestArchitecture : public Architecture {
  Device & camera = mk_component< Device >("camera");
  SharedData<Image> & color = mk_resource< SharedData<Image> >("image_color", Image());
  SharedData<Image> & depth = mk_resource< SharedData<Image> >("image_depth", Image());

  bool configure_hook() override {
    camera.shell().color.connect(color.interface().write);
    camera.shell().depth.connect(depth.interface().write);
    camera.fsm().period = ms_to_ns(10);

    return Architecture::configure_hook();
  }
};

int main() {
  std::stringstream config;
  config << "default:" << std::endl;
  config << "  type: stdout" << std::endl;
  config << "  level: trace" << std::endl;
  AbstractLogger::initialize(config);

  OpenNITestArchitecture* archi = new OpenNITestArchitecture();
  auto depl = mk_deployer(archi);
  archi->configure();
  depl->create_tasks();
  depl->activate();
  depl->start();

  depl->loop();

  depl->stop();
  archi->cleanup();

  return 0;
}
