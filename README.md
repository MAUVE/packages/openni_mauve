# OpenNIL-MAUVE

This package provides MAUVE components to use depth camera through OpenNI.

It is licensed under the [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0).

## Instal Instructions

You first need to have installed the MAUVE [Toolchain](https://gitlab.com/mauve/mauve_toolchain).

To install the OpenNI-MAUVE package in the MAUVE workspace:
```
cd ~/mauve_ws
git clone https://gitlab.com/MAUVE/openni_mauve.git src/openni_mauve
catkin_make
```

## Documentation

### Device component

The Device component has a PeriodicStateMachine, and provides as interface:

* an output port _color_ with the grabbed color image
* an output port _depth_ with the grabbed depth image
* a property _id_ with the camera ID to connect to ("" connects to the first camera found)

### Testing

To test the connection with the camera:
```
rosrun openni_mauve openni_mauve_component
```
